package mx.edu.ittepic.practicau3_4_imagenesremotas_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    Spinner imagenes;
    Button mostrar;
    ImageView imagen;
    ConexionWeb conexionweb;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagenes = findViewById(R.id.imagenes);
        mostrar = findViewById(R.id.mostrar);
        imagen = findViewById(R.id.imagen);

        String[] stringimagenes = {"Esfera 1", "Esfera 2", "Esfera 3", "Esfera 4", "Esfera 5"};
        ArrayAdapter<String> entries = new ArrayAdapter<String>(this, R.layout.item_spinner, stringimagenes);
        imagenes.setAdapter(entries);

        mostrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try {
                    conexionweb = new ConexionWeb(MainActivity.this);
                    URL direccion = null;
                    switch (imagenes.getSelectedItemPosition()) {
                        case 0:
                            direccion = new URL("https://tpdmagustin.000webhostapp.com/esfera1.jpg");
                            break;
                        case 1:
                            direccion = new URL("https://tpdmagustin.000webhostapp.com/esfera2.png");
                            break;
                        case 2:
                            direccion = new URL("https://tpdmagustin.000webhostapp.com/esfera3.png");
                            break;
                        case 3:
                            direccion = new URL("https://tpdmagustin.000webhostapp.com/esfera4.png");
                            break;
                        case 4:
                            direccion = new URL("https://tpdmagustin.000webhostapp.com/esfera5.png");
                            break;
                    }
                    dialog = ProgressDialog.show(MainActivity.this, "Atención", "Conectando con el servidor");
                    conexionweb.execute(direccion);
                }catch(MalformedURLException e){
                    Toast.makeText(getApplicationContext(), "ERROR: dirección incorrecta", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void procesarRespuesta(Bitmap img){
        dialog.dismiss();
        if (img != null) {
            imagen.setImageBitmap(img);
        }else{
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("ADVERTENCIA").setMessage("Error: no hay imágen para mostrar")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        }
    }
    public void mostrarMensaje(String msj){
        dialog.setMessage(msj);
    }
}
